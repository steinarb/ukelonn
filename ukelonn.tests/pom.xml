<?xml version="1.0" encoding="UTF-8"?>
<!-- Copyright 2016-2017 Steinar Bang                                                -->
<!--                                                                                 -->
<!-- Licensed under the Apache License, Version 2.0 (the "License");                 -->
<!-- you may not use this file except in compliance with the License.                -->
<!-- You may obtain a copy of the License at                                         -->
<!--   http://www.apache.org/licenses/LICENSE-2.0                                    -->
<!-- Unless required by applicable law or agreed to in writing,                      -->
<!-- software distributed under the License is distributed on an "AS IS" BASIS,      -->
<!-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.        -->
<!-- See the License for the specific language governing permissions and limitations -->
<!-- under the License.                                                              -->

<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd" xml:space="preserve">
    <modelVersion>4.0.0</modelVersion>
    <parent>
        <artifactId>parent</artifactId>
        <groupId>no.priv.bang.ukelonn</groupId>
        <version>1.0.0-SNAPSHOT</version>
    </parent>

    <artifactId>ukelonn.tests</artifactId>

    <properties>
        <logback.version>1.0.4</logback.version>
        <slf4j.version>1.7.4</slf4j.version>
        <pax.exam.framework>equinox</pax.exam.framework>
        <pax.exam.container>native</pax.exam.container>
    </properties>

    <dependencies>
        <dependency>
            <groupId>org.ops4j.pax.exam</groupId>
            <artifactId>pax-exam-container-karaf</artifactId>
        </dependency>
        <dependency>
            <groupId>org.apache.karaf</groupId>
            <artifactId>apache-karaf-minimal</artifactId>
            <type>zip</type>
        </dependency>
        <dependency>
            <groupId>org.apache.karaf.features</groupId>
            <artifactId>standard</artifactId>
            <type>xml</type>
            <classifier>features</classifier>
        </dependency>
        <dependency>
            <groupId>org.ops4j.pax.jdbc</groupId>
            <artifactId>pax-jdbc-features</artifactId>
            <version>${dependency.pax.jdbc.version}</version>
            <type>xml</type>
            <classifier>features</classifier>
        </dependency>
        <dependency>
            <groupId>org.apache.shiro</groupId>
            <artifactId>shiro-core</artifactId>
        </dependency>
        <dependency>
            <groupId>org.ops4j.pax.exam</groupId>
            <artifactId>pax-exam-junit4</artifactId>
        </dependency>
        <dependency>
            <groupId>org.ops4j.pax.exam</groupId>
            <artifactId>pax-exam</artifactId>
            <version>${dependency.pax.exam.version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.ops4j.pax.exam</groupId>
            <artifactId>pax-exam-link-mvn</artifactId>
        </dependency>
        <dependency>
            <groupId>org.apache.geronimo.specs</groupId>
            <artifactId>geronimo-atinject_1.0_spec</artifactId>
            <version>1.0</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>org.osgi</groupId>
            <artifactId>org.osgi.core</artifactId>
        </dependency>
        <dependency>
            <artifactId>karaf</artifactId>
            <groupId>no.priv.bang.ukelonn</groupId>
            <version>${project.version}</version>
            <type>pom</type>
        </dependency>
        <dependency>
            <groupId>no.priv.bang.ukelonn</groupId>
            <artifactId>ukelonn.api</artifactId>
            <version>${project.version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>no.priv.bang.ukelonn</groupId>
            <artifactId>ukelonn.bundle</artifactId>
            <version>${project.version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>no.priv.bang.ukelonn</groupId>
            <artifactId>ukelonn.bundle.db.liquibase</artifactId>
            <version>${project.version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>no.priv.bang.ukelonn</groupId>
            <artifactId>ukelonn.bundle.db.test</artifactId>
            <version>${project.version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.ops4j.pax.web.itest</groupId>
            <artifactId>pax-web-itest-base</artifactId>
            <version>${dependency.pax.web.version}</version>
            <scope>provided</scope>
        </dependency>
    </dependencies>

    <build>
        <testResources>
            <testResource>
                <directory>src/test/resources</directory>
                <filtering>true</filtering>
            </testResource>
        </testResources>
        <plugins>
            <!-- generate dependencies versions for use by asInProject() in pax exam tests -->
            <plugin>
                <groupId>org.apache.servicemix.tooling</groupId>
                <artifactId>depends-maven-plugin</artifactId>
                <executions>
                    <execution>
                        <id>generate-depends-file</id>
                        <goals>
                            <goal>generate-depends-file</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-surefire-plugin</artifactId>
            </plugin>
        </plugins>
    </build>
</project>
