package no.priv.bang.ukelonn.bundle.db.postgresql;

import no.priv.bang.ukelonn.bundle.db.liquibase.UkelonnLiquibase;

public interface UkelonnLiquibaseFactory {

    UkelonnLiquibase create();

}
